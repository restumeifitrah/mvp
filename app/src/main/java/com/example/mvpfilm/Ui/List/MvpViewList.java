package com.example.mvpfilm.ui.list;

import com.example.mvpfilm.data.server.Data;

import java.util.List;

public interface MvpViewList {
    void onGetResult(List<Data.Search> data);
}
