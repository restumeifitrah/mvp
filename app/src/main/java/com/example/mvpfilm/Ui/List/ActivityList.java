package com.example.mvpfilm.ui.list;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

import com.example.mvpfilm.data.server.Data;
import com.example.mvpfilm.R;

import java.util.List;


public class ActivityList extends AppCompatActivity implements MvpViewList {

    private CustomAdapter adapter;
    private RecyclerView recyclerView;
    private TextView textFilmNotFound;

    PresenterList mPresenter ;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPresenter = new PresenterList (this);

        recyclerView = (findViewById(R.id.rec));
        textFilmNotFound= findViewById(R.id.textNotFound);
        recyclerView.setAdapter(adapter);

        SearchView simpleSearchView = findViewById(R.id.simpleSearchView);
        simpleSearchView.setQueryHint("Search Film");

        simpleSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {

                return false;

            }

            @Override
            public boolean onQueryTextChange(String s) {
                mPresenter.searchByQuery(s);
                return false;
            }


        });
    }

    @Override
    public void onGetResult(List<Data.Search> searchList) {

        if(searchList != null ) {
            recyclerView.setVisibility(View.VISIBLE);
            textFilmNotFound.setVisibility(View.GONE);

            adapter = new CustomAdapter(this, searchList);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ActivityList.this);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

        }else {
            recyclerView.setVisibility(View.GONE);
            textFilmNotFound.setVisibility(View.VISIBLE);

        }
    }
}