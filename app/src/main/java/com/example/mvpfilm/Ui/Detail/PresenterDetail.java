package com.example.mvpfilm.ui.detail;

import com.example.mvpfilm.data.DataManager;
import com.example.mvpfilm.data.server.Movie;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PresenterDetail extends ActivityDetail {
    MvpViewDetail mvpViewDetail ;
    DataManager dataManager;

    public PresenterDetail(MvpViewDetail mvpViewDetail){
        this.mvpViewDetail = mvpViewDetail;
        dataManager = new DataManager();
    }

    public void searchByQuery (final String t) {


        dataManager.getDetail(t).enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                if (response.isSuccessful()) {
                    mvpViewDetail.setView(response);
                }
            }


            @Override
            public void onFailure(Call<Movie> call, Throwable t) {

            }
        });
    }
}

