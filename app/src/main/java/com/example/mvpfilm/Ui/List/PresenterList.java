package com.example.mvpfilm.ui.list;

import com.example.mvpfilm.data.DataManager;
import com.example.mvpfilm.data.server.Data;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PresenterList extends ActivityList{
    MvpViewList mvpViewList;
    DataManager dataManager;

    public PresenterList(ActivityList mpvViewList) {
        this.mvpViewList = mpvViewList;
        dataManager = new DataManager();
    }
    public void searchByQuery(final String s) {

        dataManager.getAllPhotos(s).enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                Data data = response.body();
                mvpViewList.onGetResult(data.getSearch());
            }

            @Override
            public void onFailure(Call<Data> call, Throwable s) {

            }
        });

    }
}
