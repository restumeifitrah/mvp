package com.example.mvpfilm.ui.detail;

import com.example.mvpfilm.data.server.Movie;

import retrofit2.Response;

public interface MvpViewDetail {
    void setView(Response<Movie> response);

}
