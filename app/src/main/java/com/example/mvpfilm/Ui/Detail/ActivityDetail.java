package com.example.mvpfilm.ui.detail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mvpfilm.data.server.Movie;
import com.example.mvpfilm.R;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import retrofit2.Response;

public class ActivityDetail extends AppCompatActivity implements MvpViewDetail{

     TextView genre;
     TextView runtime ;
     TextView year;
     TextView detail;

     PresenterDetail mPresenter;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();

         mPresenter = new PresenterDetail(this);

         ImageView poster = (ImageView) findViewById(R.id.imageView3) ;
         TextView judul = (TextView) findViewById(R.id.dTitle);
         genre = (TextView) findViewById(R.id.dType);
         runtime = (TextView) findViewById(R.id.dDuration);
         year = (TextView) findViewById(R.id.dYear);
         detail = (TextView) findViewById(R.id.dDetail);

        judul.setText(intent.getStringExtra("title"));
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttp3Downloader(this));
        builder.build().load(intent.getStringExtra("image"))
                .placeholder((R.drawable.ic_launcher_background))
                .error(R.drawable.ic_launcher_background)
                .into(poster);

        mPresenter.searchByQuery(intent.getStringExtra("title"));

    }

    @Override
    public void setView(Response<Movie> response) {
        genre.setText(response.body().getGenre());
        runtime.setText(response.body().getRuntime());
        year.setText(response.body().getYear());
        detail.setText(response.body().getPlot());
    }
}
