package com.example.mvpfilm.Ui.List;

import com.example.mvpfilm.Data.Server.Data;

import java.util.List;

public interface MpvViewList {
    void onGetResult(List<Data.Search> data);
}
