package com.example.mvpfilm.data.server;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetDataService {

    @GET("/")
    Call<Data> getAllPhotos(
            @Query("apikey") String api,
            @Query("s") String search

    );

    @GET("/")
    Call<Movie> getDetail(
            @Query("apikey") String api,
            @Query("t") String search

    );
}
