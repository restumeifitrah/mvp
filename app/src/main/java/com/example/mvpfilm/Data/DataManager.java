package com.example.mvpfilm.data;

import com.example.mvpfilm.data.server.Data;
import com.example.mvpfilm.data.server.GetDataService;
import com.example.mvpfilm.data.server.Movie;
import com.example.mvpfilm.util.Extra;

import retrofit2.Call;

public class DataManager {

    private GetDataService getDataService;

    public DataManager(){
        getDataService = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
    }

    public Call<Data> getAllPhotos(String s) {
        return getDataService.getAllPhotos(Extra.API_KEY, s);

    }
    public Call<Movie> getDetail (String t) {
        return getDataService.getDetail(Extra.API_KEY, t);

    }
}